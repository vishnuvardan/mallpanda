Project name : Mall Panda
Version: V1
Genre : Shopping site
Description: Products matrix, where user can list out all items, filter by categories and sort by attributes.

RWD:
* Overall Single column layout.
* products grid 3x3. Made sure, it shows 3 items in a row in all display.

Functionaly Overview:
* Loads page with 9 items.
* Clicking on Load More loads 9 more items.
* Selecting/Unselecting category items
**** Resets the loaded items to 9.
**** Filters with selected categories.
* Selecting the sort type 
**** Resets the loaded items to 9.
**** Sorts the items, which are already displayed.
* Clicking on Load More loads 9 more items, with being category selected and sorting selected.

Known Issues:
* Image urls from the APIs, loads slowly or ends up not loading at all. So for workaround, hardcoded image is used. Search for "images/jacket.jpg" in productMatrix.js file.
* API is working correctly from the local HTML file. It might not load correctly in every system. In that case, this project needs to be run in a server.