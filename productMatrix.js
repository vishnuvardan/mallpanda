var mallPanda = {
    itemsStart: 0,
    itemsInAPage: 9,
    itemsInARow: 3,
    selectedCategories: [],
    sortType: "",
    loadedJSON: [],
    filteredJson: [],
    holder: {},
    loader: {},
    init: function () {
        this.holder = document.getElementsByClassName('productsWrapper')[0];
        this.loader = document.getElementsByClassName('loader')[0].classList;
        this.loadJSON();
        this.addEventListeners();
    },
    /**
     * initScroll - Sets the occupying height on loadJSON and window resize, to enable virtual scrolling
     */
    initScroll: function () {
        this.holder.style.height = 'auto';
        this.holder.style.height = this.holder.clientHeight + 'px';
    },
    /**
     * loadJSON - pulls the JSON from the API Server
     */
    loadJSON: function () {
        var xhttp = new XMLHttpRequest();
        var thisObj = this;
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    thisObj.loadedJSON = JSON.parse(this.responseText);
                    if (thisObj.loadedJSON.products) {
                        thisObj.loadedJSON = thisObj.loadedJSON.products;
                        thisObj.filteredJson = thisObj.loadedJSON;
                        for (index = 0 ; index < thisObj.itemsInAPage; index+=thisObj.itemsInARow){
                            thisObj.loadMore(index, index + thisObj.itemsInARow);
                        }
                    }
                } else {
                    alert("Something went wrong, please try again!");
                }
                thisObj.loader.add('hide');
                thisObj.initScroll();
            }
        };
        xhttp.open("GET", "https://test-prod-api.herokuapp.com/products", true);
        xhttp.send();

    },
    /**
     * prepareDom - takes one item from the JSON array and 
     * returns HTML structure
     */
    prepareDom: function (thisElement) {
        var dom = "<div class='imageWrapper' style='background-image: url("+ thisElement.img +")'>";
        // var dom = "<div class='imageWrapper' style='background-image: url(images/jacket.jpg)'>";
        dom += "<div class='category'>" + thisElement.cat + "</div>";
        dom += "</div>";
        dom += "<div class='productDesc'>";
        dom += "<a href=" + thisElement.url + " class='productName'>" + thisElement.name + "</a>";
        dom += "<div class='mall-space-around'><p class='productPrice'><span class='productCurrency'>Rs. </span>" + thisElement.price + "</p>";
        dom += "<p class='productScore'>" + thisElement.score + "</p></div>";
        dom += "</div>";

        return dom;
    },
    /**
     * loadMore - takes care of pagination
     * start - from which element to render
     * end - till which element to render
     */
    loadMore: function (start, end, invert) {
        var productRow = document.createElement('div');
        productRow.className = "productRow";
        for (var index = start; index < end; index++) {
            var element = mallPanda.filteredJson[index];

            var productItem = document.createElement('div');
            productItem.className = "productItem";
            productItem.id = element.id;
            productItem.innerHTML = this.prepareDom(element);

            productRow.appendChild(productItem);
        }
        if (invert)
            this.holder.insertBefore(productRow, this.holder.childNodes[0]);
        else
            this.holder.appendChild(productRow);
    },
    /**
     * getId - returns the index of item from the sorted/filtered array
     * later helps to fetch 3+ or 3- items.
     */
    getId: function(targetId) {
        var result = 0;
        for (var index = 0 ; index < mallPanda.filteredJson.length; index++) {
            var element = mallPanda.filteredJson[index];
            if (element.id == targetId){
                result = index;
                break;
            }
        }
        return result;
    },
    /**
     * appendUp - Pulls 3 items before the first item and pushes to dom and animates scrolling.
     */
    appendUp: function() {
        var allItems = document.getElementsByClassName('productItem');
        var firstItem = parseInt(allItems[0].id);
        var firstVisibleItemInJSON = mallPanda.getId(firstItem);
        
        if (firstVisibleItemInJSON <= 0 ) {
            firstVisibleItemInJSON = mallPanda.filteredJson.length;
        }
        mallPanda.loadMore(firstVisibleItemInJSON-mallPanda.itemsInARow , firstVisibleItemInJSON, 1);

        var productRows = document.getElementsByClassName('productRow');
        mallPanda.holder.scrollTop = productRows[0].clientHeight;
        var scrollIndex = productRows[0].clientHeight;
        var intervalId = setInterval(function(){
            mallPanda.holder.scrollTop = scrollIndex;
            scrollIndex-=5;
            if (scrollIndex < 0) {
                clearInterval(intervalId);
                productRows[productRows.length - 1].remove();
            }
        }, 1);
    },
    /**
     * appendDown - Pulls 3 items after the last item and pushes to dom and animates scrolling.
     */
    appendDown: function() {
        var allItems = document.getElementsByClassName('productItem');
        var lastItem = parseInt(allItems[allItems.length-1].id);
        var lastVisibleItemInJSON = mallPanda.getId(lastItem) + 1;

        if (lastVisibleItemInJSON >= mallPanda.filteredJson.length){
            lastVisibleItemInJSON = 0;
        }
        mallPanda.loadMore(lastVisibleItemInJSON, lastVisibleItemInJSON + mallPanda.itemsInARow);

        var productRows = document.getElementsByClassName('productRow');
        var scrollIndex = 0;
        var intervalId = setInterval(function(){
            mallPanda.holder.scrollTop = scrollIndex;
            scrollIndex+=5;
            if (scrollIndex > productRows[1].clientHeight) {
                clearInterval(intervalId);
                productRows[0].remove();
            }
        }, 1);
    },
    /**
     * mouseWheel - Mouse Wheel event to trigger appendDown or appendUp
     */
    mouseWheel: function(e) {
        if (e.wheelDeltaY > 0) {
            mallPanda.appendUp();
        }
        else {
            mallPanda.appendDown();
        }
    },
    /**
     * updateCategory - updates the selected category to the global variable - selectedCategories
     */
    updateCategory: function (e) {
        mallPanda.filteredJson = [];
        mallPanda.selectedCategories = [];
        var checkBoxes = document.getElementsByClassName('selectCategory');
        for (var index = 0; index < checkBoxes.length; index++) {
            var element = checkBoxes[index];
            if (element.checked) {
                mallPanda.selectedCategories.push(element.value);
            }
        }
        mallPanda.renderViaFilters();
    },
    /**
     * sortBy - updates sortType to the global variable - sortType
     */
    sortBy: function (e) {
        mallPanda.sortType = e.target.value;
        mallPanda.renderViaFilters();
    },
    /**
     * renderViaFilters
     * fetches selectedCategories and filters
     * fetches sortType and Sorts
     */
    renderViaFilters: function () {
        mallPanda.holder.innerHTML = "";
        mallPanda.itemsStart = 0;
        mallPanda.filteredJson = [];

        if (mallPanda.selectedCategories.length == 0) {
            mallPanda.filteredJson = mallPanda.loadedJSON;
        } else {
            for (var index = 0; index < mallPanda.loadedJSON.length; index++) {
                var element = mallPanda.loadedJSON[index];
                for (var indexCategory = 0; indexCategory < mallPanda.selectedCategories.length; indexCategory++) {
                    var elementCategory = mallPanda.selectedCategories[indexCategory];
                    if (elementCategory == element.cat) {
                        mallPanda.filteredJson.push(element);
                    }
                }
            }
        }

        mallPanda.filteredJson.sort(function (a, b) {
            return a[mallPanda.sortType] - b[mallPanda.sortType]
        });
        
        for (index = 0 ; index < mallPanda.itemsInAPage; index+=mallPanda.itemsInARow){
            mallPanda.loadMore(index, index + mallPanda.itemsInARow);
        }

    },
    /**
     * addEventListers 
     * adds events to category checkboxes, sort dropdown and load more pagination
     */
    addEventListeners: function () {
        var checkBoxes = document.getElementsByClassName('selectCategory');
        for (var index = 0; index < checkBoxes.length; index++) {
            var element = checkBoxes[index];
            element.addEventListener('click', this.updateCategory);
        }
        document.getElementsByClassName('selectSort')[0].addEventListener('change', this.sortBy);
        document.getElementsByClassName('loadUp')[0].addEventListener('click', this.appendUp);
        document.getElementsByClassName('loadDown')[0].addEventListener('click', this.appendDown);
        document.getElementsByClassName('productsWrapper')[0].addEventListener('wheel', this.mouseWheel);
    }
}

/* Initiate Init on DOM ready */
document.addEventListener("DOMContentLoaded", function (event) {
    mallPanda.init();
});

/* Initiate Init Scroll on Window Resize */
window.addEventListener("resize", function (event) {
    mallPanda.initScroll();
});